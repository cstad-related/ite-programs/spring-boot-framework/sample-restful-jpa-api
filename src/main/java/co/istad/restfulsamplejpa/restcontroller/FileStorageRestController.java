package co.istad.restfulsamplejpa.restcontroller;


import co.istad.restfulsamplejpa.service.FileStorageService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/files")
@RequiredArgsConstructor
public class FileStorageRestController {
    private final FileStorageService fileStorageService;


    private String generateImageUrl(String filename, HttpServletRequest request) {
        return String.format("%s://%s:%d/images/%s"
                , request.getScheme(),
                request.getServerName(),
                request.getServerPort(),
                filename);
    }

    @PostMapping(value = "", consumes = {"multipart/form-data"})
    @ResponseStatus(HttpStatus.CREATED)
    public HashMap<String, Object> uploadFile(@RequestPart("file") MultipartFile file, HttpServletRequest request) {
        generateImageUrl("filename.jpg", request);
        HashMap<String, Object> response = new HashMap<>();
        response.put("message","Successfully upload the file ");

        response.put("payload",
                generateImageUrl(
                        fileStorageService.uploadSingleFile(file),
                        request
                )
        )
        ;
        response.put("status",HttpStatus.CREATED.value());
        return response;
    }


    @PostMapping(value = "/multiple", consumes = {"multipart/form-data"})
    public HashMap<String, Object> uploadMultipleFile(@RequestPart("files") MultipartFile[] files) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("message", "Successfully Uploaded Images !");
        response.put("status", HttpStatus.CREATED.value());
        response.put("payload", fileStorageService.uploadMultipleFiles(files));
        return response;
    }



    @DeleteMapping("/{filename}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public HashMap<String,Object> deleteFile(@PathVariable String filename){
         fileStorageService.deleteFileByName(filename);
        return null;
    }

}
