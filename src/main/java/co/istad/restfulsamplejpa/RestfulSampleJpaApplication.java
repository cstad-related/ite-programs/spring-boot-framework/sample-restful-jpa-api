package co.istad.restfulsamplejpa;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(title = "Data Analytic Restful API", description = "This is a free api for public usage!", contact = @Contact(name = "James Gosling", email = "james@gmail.com", url = "www.facebook.com/james"))
)

public class RestfulSampleJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfulSampleJpaApplication.class, args);
    }

}
